Ansible Role to manage Grafana installation
-------------------------------------------

This simple role allow you to install Grafana on a specific machine.

## Example

```ansible-playbook
- name: Install Grafana
  include_role:
    name: grafana
  vars:
    domain: "grafana.domain.ltd"
    root_url: 'https://grafana.domain.ltd'
```
